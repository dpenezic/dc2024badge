#!/bin/bash

# get sch revision
REV=$(grep "(rev" DCBadge24.kicad_sch |cut -d'"' -f 2)
OUTDIR=production/$(date +%Y%m%d)-${REV}

mkdir -p $OUTDIR

# generate gerbers, BOM and position files for JLCPCB
kikit fab jlcpcb \
  --nametemplate "DC2024badge-{}" \
  --assembly \
  --schematic DCBadge24.kicad_sch \
  --field "JLCPCB#" \
  panel/DCBadge24-panel.kicad_pcb $OUTDIR

# export the schematic as pdf
kicad-cli sch export pdf --output ${OUTDIR}/DCBadge24-schematic.pdf DCBadge24.kicad_sch

# copy the source pcb files
cp panel/DCBadge24-panel.kicad_pcb $OUTDIR
cp DCBadge24.kicad_pcb $OUTDIR
