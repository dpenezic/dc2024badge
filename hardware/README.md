# DORS/CLUC 2024 electronic badge hardware files

Kicad files for the [DORS/CLUC 2024](https://www.dorscluc.org/) electronic badge.

More details about the badge functionality will be posted soon.


# Generating production files

- Run `make_panel.sh` to generate the panelized Kicad project in the `panel` directory.
- Run `make_prod.sh` to generate the production files (gerbers, BOM, CPL, PnP) in the `production/${date}-${rev}` directory.


## License

Copyright HYPERGLITCH Ltd 2024.

The hardware files are licensed under the CERN-OHL-P v2 license. See the `LICENSE` file for details.


