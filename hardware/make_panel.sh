#!/bin/bash


mkdir -p panel

kikit panelize \
  --layout 'grid; rows: 1; cols: 2; space: 5mm; vbackbone: 7mm' \
  --tabs annotation --source 'tolerance: 15mm' \
  --post 'millradius: 1.2mm' \
  --cuts 'mousebites; drill: 0.5mm; spacing: 0.7mm; offset: -0.2mm; prolong: 0mm' \
  --framing 'frame; width: 7mm; space: 4mm;cuts: h' \
  --tooling '3hole; hoffset: 2.5mm; voffset: 2.5mm; size: 1.152mm' \
  --fiducials '3fid; hoffset: 5mm; voffset: 2.5mm; coppersize: 2mm; opening: 1mm;' \
  --text 'simple; text: DC2024 badge, order# JLCJLCJLCJLC; voffset: 2.5mm; justify: center; vjustify: center;' \
  --copperfill 'hatched; clearance: 2mm; spacing: 0.5mm; width: 0.5mm' \
  DCBadge24.kicad_pcb panel/DCBadge24-panel.kicad_pcb


