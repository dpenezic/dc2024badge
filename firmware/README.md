# DORS/CLUC 2024 badge firmware

This repository contains the firmware for the DORS/CLUC 2024 badge.

## License

Project contains code with multiple licenses.

- `Core/Src/main.c` contains STM32CubeIDE generated code and the code for the badge itself. This code is licensed under the MIT license. See the LICENSE file for details.
- `Drivers/STM32F4xx_HAL_Driver` contains the STM32 HAL library. This code is licensed under the BSD 3-Clause license. See the LICENSE file in `Drivers/STM32F4xx_HAL_Driver` for details.
- `Drivers/CMSIS` contains the CMSIS library. This code is licensed under the Apache 2.0 license. See the LICENSE file in `Drivers/CMSIS` for details.
- `Core/Wavesshare` contains the Waveshare e-paper library. Code is published and free to use but the license is not specified.

Badge code copyright (c) 2024 Igor Brkic <igor@hyperglitch.com>

