/*
 * DEV_Config.h
 *
 *  Created on: Mar 6, 2024
 *      Author: i
 */

// mostly copied from the Waveshare's DEV_Config.c file in order to adjust
// properties without having to change other Waveshare files


#ifndef INC_DEV_CONFIG_H_
#define INC_DEV_CONFIG_H_

#include <stdint.h>
#include "main.h"

#define UBYTE   uint8_t
#define UWORD   uint16_t
#define UDOUBLE uint32_t

#define EPD_RST_PIN     EPD_RST_GPIO_Port, EPD_RST_Pin
#define EPD_DC_PIN      EPD_DC_GPIO_Port, EPD_DC_Pin
#define EPD_PWR_PIN     EPD_EN_GPIO_Port, EPD_EN_Pin
#define EPD_CS_PIN      SPI1_CS_GPIO_Port, SPI1_CS_Pin
#define EPD_BUSY_PIN    EPD_BUSY_GPIO_Port, EPD_BUSY_Pin

#define DEV_Digital_Write(_pin, _value) HAL_GPIO_WritePin(_pin, _value == 0? GPIO_PIN_RESET:GPIO_PIN_SET)
#define DEV_Digital_Read(_pin) HAL_GPIO_ReadPin(_pin)

#define DEV_Delay_ms(__xms) HAL_Delay(__xms);

void DEV_SPI_WriteByte(UBYTE value);
void DEV_SPI_Write_nByte(UBYTE *value, UDOUBLE len);

int DEV_Module_Init(void);
void DEV_Module_Exit(void);


#endif /* INC_DEV_CONFIG_H_ */
