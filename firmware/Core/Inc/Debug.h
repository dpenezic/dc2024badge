/*
 * Debug.h
 *
 *  Created on: Mar 6, 2024
 *      Author: i
 */

#ifndef INC_DEBUG_H_
#define INC_DEBUG_H_

#include<stdio.h>

#if 1
	#define Debug(__info,...) printf("Debug: " __info,##__VA_ARGS__)
#else
	#define Debug(__info,...)
#endif


#endif /* INC_DEBUG_H_ */
