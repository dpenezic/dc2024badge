/*
 * eink_images.h
 *
 *  Created on: May 10, 2024
 *      Author: i
 */

#ifndef INC_EINK_IMAGES_H_
#define INC_EINK_IMAGES_H_

extern const unsigned char gImage_block[];
extern const unsigned char gImage_robot[];
extern const unsigned char gImage_thumbup[];
extern const unsigned char gImage_heart[];
extern const unsigned char gImage_save[];
extern const unsigned char gImage_image[];

#endif /* INC_EINK_IMAGES_H_ */
