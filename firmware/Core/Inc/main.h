/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_ROW_1_Pin GPIO_PIN_0
#define LED_ROW_1_GPIO_Port GPIOA
#define TOUCH_KEY_Pin GPIO_PIN_4
#define TOUCH_KEY_GPIO_Port GPIOA
#define LIGHT_DETECT_Pin GPIO_PIN_6
#define LIGHT_DETECT_GPIO_Port GPIOA
#define IR_RECV_Pin GPIO_PIN_1
#define IR_RECV_GPIO_Port GPIOB
#define IR_RECV_EXTI_IRQn EXTI1_IRQn
#define IR_RX_EN_Pin GPIO_PIN_2
#define IR_RX_EN_GPIO_Port GPIOB
#define LED_ROW_3_Pin GPIO_PIN_10
#define LED_ROW_3_GPIO_Port GPIOB
#define LED_ROW_4_Pin GPIO_PIN_11
#define LED_ROW_4_GPIO_Port GPIOB
#define LED_COL_2_Pin GPIO_PIN_12
#define LED_COL_2_GPIO_Port GPIOB
#define LED_COL_3_Pin GPIO_PIN_13
#define LED_COL_3_GPIO_Port GPIOB
#define LED_COL_4_Pin GPIO_PIN_14
#define LED_COL_4_GPIO_Port GPIOB
#define LED_COL_1_Pin GPIO_PIN_15
#define LED_COL_1_GPIO_Port GPIOB
#define IR_TX_Pin GPIO_PIN_8
#define IR_TX_GPIO_Port GPIOA
#define SPI1_CS_Pin GPIO_PIN_10
#define SPI1_CS_GPIO_Port GPIOA
#define EPD_EN_Pin GPIO_PIN_12
#define EPD_EN_GPIO_Port GPIOA
#define LED_ROW_2_Pin GPIO_PIN_3
#define LED_ROW_2_GPIO_Port GPIOB
#define EPD_BUSY_Pin GPIO_PIN_4
#define EPD_BUSY_GPIO_Port GPIOB
#define EPD_RST_Pin GPIO_PIN_5
#define EPD_RST_GPIO_Port GPIOB
#define EPD_DC_Pin GPIO_PIN_6
#define EPD_DC_GPIO_Port GPIOB
#define LED_COL_7_Pin GPIO_PIN_7
#define LED_COL_7_GPIO_Port GPIOB
#define BOOT0_Pin GPIO_PIN_3
#define BOOT0_GPIO_Port GPIOH
#define LED_COL_6_Pin GPIO_PIN_8
#define LED_COL_6_GPIO_Port GPIOB
#define LED_COL_5_Pin GPIO_PIN_9
#define LED_COL_5_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
