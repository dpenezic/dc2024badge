/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "instance_id.h"  // instance ID and firmware version

// Waveshare code
#include "DEV_Config.h"
#include "../Waveshare/GUI/GUI_Paint.h"

// when converting the images the max width and height in Image2Lcd can bi greater than
// the actual image. For the base image 114x152 wouldn't work but setting the max size
// to 128x152 and keeping the image as 114x152 (and using that in the code) worked
#include "eink_images.h"

#define EINK_DISPLAY_2IN66_MONOCHROMATIC
//#define EINK_DISPLAY_2IN66_TWOCOLOR

#ifdef EIN_DISPLAY_2IN66_TWOCOLOR
	#include "../Waveshare/e-Paper/EPD_2in66b.h"
	#define DISPLAY_WIDTH (EPD_2IN66B_WIDTH)
	#define DISPLAY_HEIGHT (EPD_2IN66B_HEIGHT)

	#define DisplayInit EPD_2IN66B_Init
	#define DisplayClear EPD_2IN66B_Clear
	#define DisplaySleep EPD_2IN66B_Sleep
	#define DisplayShow EPD_2IN66B_Display
#else
	#include "../Waveshare/e-Paper/EPD_2in66.h"
	#define DISPLAY_WIDTH (EPD_2IN66_WIDTH)
	#define DISPLAY_HEIGHT (EPD_2IN66_HEIGHT)

	#define DisplayInit EPD_2IN66_Init
	#define DisplayClear EPD_2IN66_Clear
	#define DisplaySleep EPD_2IN66_Sleep
	#define DisplayShow EPD_2IN66_Display
#endif

// statically allocate memory
#define __image_size (((DISPLAY_WIDTH % 8 == 0)? (DISPLAY_WIDTH / 8 ): (DISPLAY_WIDTH / 8 + 1)) * DISPLAY_HEIGHT)
UBYTE imgdata_black[__image_size] = {0};
UBYTE imgdata_red[__image_size] = {0};

//Create a new image cache
UBYTE *BlackImage = imgdata_black;
UBYTE *RedImage = imgdata_red;

volatile const char __author[]  = "Igor Brkic";
volatile const char __company[] = "HYPERGLITCH Ltd";
volatile const char __url[]     = "https://hyperglitch.com";

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim7;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM2_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM7_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


int __io_putchar(int c){
        const uint8_t cc[1]={(uint8_t)c};
        HAL_UART_Transmit(&huart2, cc, 1, 100);
        //HAL_UART_Transmit_IT(&huart1, cc, 1);
        return 0;
}

typedef struct{
	GPIO_TypeDef* port;
	uint16_t pin;
} pin_t;


// flags are stuffed into upper 4 bits of the frame start mark
typedef enum{
	DEVFLAG_IS_CONFIG = 1<<4,
	DEVFLAG_HIDE_QUEST = 1<<5,
	DEVFLAG_IR_SEND_DISABLE = 1<<6,
	DEVFLAG_IR_RECV_DISABLE = 1<<7,
} dev_flags_t;


// LED matrix rows are connected to PWM enabled pins on TIM2
const uint32_t led_rows_num = 4;
/*
const uint32_t led_rows[] = {
		TIM_CHANNEL_1,
		TIM_CHANNEL_2,
		TIM_CHANNEL_3,
		TIM_CHANNEL_4,
};
*/
const pin_t led_rows[] = {
		{LED_ROW_1_GPIO_Port, LED_ROW_1_Pin},
		{LED_ROW_2_GPIO_Port, LED_ROW_2_Pin},
		{LED_ROW_3_GPIO_Port, LED_ROW_3_Pin},
		{LED_ROW_4_GPIO_Port, LED_ROW_4_Pin},
};

// LED matrix columns are connected to GPIOS configured as outputs
const uint32_t led_cols_num = 7;
const pin_t led_cols[] = {
		{LED_COL_1_GPIO_Port, LED_COL_1_Pin},
		{LED_COL_2_GPIO_Port, LED_COL_2_Pin},
		{LED_COL_3_GPIO_Port, LED_COL_3_Pin},
		{LED_COL_4_GPIO_Port, LED_COL_4_Pin},
		{LED_COL_5_GPIO_Port, LED_COL_5_Pin},
		{LED_COL_6_GPIO_Port, LED_COL_6_Pin},
		{LED_COL_7_GPIO_Port, LED_COL_7_Pin},
};

typedef struct{
	uint8_t row;
	uint8_t col;
} matrix_item_t;

const uint32_t led_num = 26;
const matrix_item_t leds_in_order[] = {
		{3, 0},
		{2, 0},
		{1, 0},
		{0, 0},
		{1, 1},

		{3, 2},
		{2, 2},
		{1, 2},
		{0, 2},

		{3, 3},
		{2, 3},
		{1, 3},
		{0, 3},
		{1, 4},

		{2, 4},
		{3, 4},
		{0, 4},

		{0, 5},
		{1, 5},
		{2, 5},
		{1, 6},
		{2, 6},
		{0, 6},

		{2, 1},
		{3, 1},
		{0, 1},
};

uint16_t led_status[26] = {0};

bool beak_touched = false;

uint16_t adc_data[4];

int led_position_to_idx(int row, int col){
	// TODO: cache this if needed
	for(int i=0; i<led_num; i++){
		if(leds_in_order[i].row==row && leds_in_order[i].col==col) return i;
	}
	return 0;
}


void led_update_isr(void){
	// in each iteration disable the current col and enable the next one (if needed)
	static int curr_row = 0;
	static int curr_col = 0;

	// disable current column
	HAL_GPIO_WritePin(led_cols[curr_col].port, led_cols[curr_col].pin, 1);

	curr_col++;
	if(curr_col>=led_cols_num){
		// activate next row
		curr_col = 0;

		HAL_GPIO_WritePin(led_rows[curr_row].port, led_rows[curr_row].pin, 0);
		curr_row++;

		if(curr_row>=led_rows_num){
			curr_row = 0;
		}
		HAL_GPIO_WritePin(led_rows[curr_row].port, led_rows[curr_row].pin, 1);
	}

	const int lidx = led_position_to_idx(curr_row, curr_col);
	if(led_status[lidx]>0){
		// enable column
		HAL_GPIO_WritePin(led_cols[curr_col].port, led_cols[curr_col].pin, 0);
	}
}

void set_all_leds(uint16_t val){
	for(int i=0; i<led_num; i++){
		led_status[i] = val;
	}
}


uint32_t get_fake_rand(){
	static uint16_t m_w = 0x1337;
	static uint16_t m_z = 0xbaba;

	m_z = 36969 * (m_z & 65535) + (m_z >> 16);
	m_w = 18000 * (m_w & 65535) + (m_w >> 16);
	return (m_z << 16) + m_w;  // 32-bit result
}


typedef struct{
	uint32_t _header;
	uint32_t _checksum;
	uint8_t text[100]; // 25*uint32_t
	uint32_t flags;
	uint32_t quest_status;
	uint32_t seen_devices[8];
	uint32_t reserved;
} device_config_t;

union device_config_union{
  device_config_t config;
  uint64_t blocks64[19];
  uint32_t blocks32[38];
};

const uint32_t data_flash_addr = 0x8000000UL + 252*1024;
const uint32_t data_config_header = 0x1337;

void configuration_init(device_config_t *conf){
	conf->_header = data_config_header;
	conf->flags = 0;
	conf->quest_status = 0;
	if(__INSTANCE_ID>=1 && __INSTANCE_ID<=12){
		conf->quest_status |= (1U<<__INSTANCE_ID);
	}
	conf->reserved = 0;
	for(int i=0; i<8; i++) conf->seen_devices[i] = 0;
	strcpy((char*)conf->text, "DORS/CLUC 2024");
}

void configuration_print(device_config_t *conf){
	printf("Configuration:\r\n");
	printf("  header: 0x%lx\r\n", conf->_header);
	printf("  checksum: 0x%lx\r\n", conf->_checksum);
	printf("  text: %s\r\n", conf->text);
	printf("  flags:\r\n");
	printf("     HIDE_QUEST: %d\r\n", (conf->flags&DEVFLAG_HIDE_QUEST)>0);
	printf("     IR_RECV_DISABLE: %d\r\n", (conf->flags&DEVFLAG_IR_RECV_DISABLE)>0);
	printf("     IR_SEND_DISABLE: %d\r\n", (conf->flags&DEVFLAG_IR_SEND_DISABLE)>0);
	printf("  quest fields discovered: ");
	for(int i=1; i<=12; i++){
		if(conf->quest_status&(1U<<i)){
			printf("%d ", i);
		}
	}
	printf("\r\n");
	printf("  bonus fields discovered: ");
	for(int i=28; i<=31; i++){
		if(conf->quest_status&(1U<<i)){
			printf("%d ", i-28);
		}
	}
	printf("\r\n");
	printf("  devices seen: ");
	for(int devidx=0; devidx<8; devidx++){
		for(int bitidx=0; bitidx<32; bitidx++){
			if(conf->seen_devices[devidx]&(1U<<bitidx)){
				printf("%d ", devidx*32+bitidx);
			}
		}
	}
	printf("\r\n");
}

bool configuration_load(device_config_t *conf){
	uint32_t address;
	union device_config_union dc;

	// load the config and calculate sum
	uint32_t sum = 0;
	address = data_flash_addr;
	for(int i=0; i<sizeof(device_config_t)/4; i++){
		dc.blocks32[i] = *(__IO uint32_t *)(address+4*i);
		if(i>=2){
			sum += dc.blocks32[i];
		}
	}

	// check if valid
	if(dc.config._header!=data_config_header){
		printf("No config found\r\n");
		return false;
	}
	if(dc.config._checksum!=sum){
		printf("Config checksum mismatch\r\n");
		return false;
	}

	// copy to main config
	*conf = dc.config;
	printf("Config loaded\r\n");
	return true;
}

bool configuration_save(device_config_t config){
	uint32_t address;
	union device_config_union dc;

	dc.config = config;

	// update config checksum
	uint32_t sum = 0;
	for(int i=2; i<sizeof(device_config_t)/4; i++){
		sum += dc.blocks32[i];
	}
	dc.config._checksum = sum;

	if(HAL_FLASH_Unlock()!=HAL_OK){
		printf("error unlocking flash\r\n");
		return false;
	}

	// erase flash page
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR); // Clear OPTVERR bit set on virgin samples (maybe not needed)

	FLASH_EraseInitTypeDef EraseInitStruct;
	uint32_t page_error = 0;
	EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
	EraseInitStruct.Page = (data_flash_addr-FLASH_BASE)/FLASH_PAGE_SIZE;
	EraseInitStruct.NbPages = 1;

	if(HAL_FLASHEx_Erase(&EraseInitStruct, &page_error) != HAL_OK){
		printf("error erasing config flash\r\n");
		HAL_FLASH_Lock();
		return false;
	}

	// write the data to the flash
	address = data_flash_addr;
	for(int i=0; i<sizeof(device_config_t)/8; i++){
		if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, address, dc.blocks64[i]) == HAL_OK){
			address = address + 8;
		}
		else{
			printf("error saving config\r\n");
			HAL_FLASH_Lock();
			return false;
		}
	}
	printf("config saved\r\n");

	HAL_FLASH_Lock();
	return true;
}


typedef enum{
	DEC_PH_INIT,
	DEC_PH_DATA,
} decoder_phase_t;

typedef enum{
	DEC_ERROR_NONE,
	DEC_ERROR_NUMBITS,
	DEC_ERROR_BITSKIP,
	DEC_ERROR_BUFFER_OVF,
} decoder_error_reason_t;

#define _decoder_buffer_size (24)
#define _decoder_entry(dec, didx) (dec->time[(dec->idx+_decoder_buffer_size+(didx))%_decoder_buffer_size])
#define _div_int_round(a, b)  (((a) + ((b)/2))/(b))


typedef struct{
	uint32_t val;
	uint32_t time[_decoder_buffer_size];
	uint32_t last_update;
	uint32_t last_update_ms;
	uint32_t idx;
	uint32_t period;
	decoder_phase_t phase;
	uint8_t data[100];
	uint8_t skip_bits;
	uint8_t manch_buff;
	uint8_t decode_tmp;
	uint8_t bit_counter;
	uint8_t data_idx;
	bool preamble_found;
	bool data_found;
	bool enabled;
	decoder_error_reason_t error;
} decoder_t;

static decoder_t ir_decoder;
static decoder_t light_decoder;

void decoder_init(decoder_t *dec){
	dec->val = 0;
	dec->period = 0;
	dec->last_update = 0;
	dec->last_update_ms = 0;
	dec->idx = 0;
	dec->data_idx = 0;
	dec->phase = DEC_PH_INIT;
	dec->skip_bits = 0;
	dec->preamble_found = false;
	dec->data_found = false;
	dec->enabled = true;
	dec->error = DEC_ERROR_NONE;
}

void print_decoder_error(decoder_error_reason_t err){
  switch(err){
  case DEC_ERROR_NONE:
	  printf("DEC_ERROR_NONE");
	  break;
  case DEC_ERROR_NUMBITS:
	  printf("DEC_ERROR_NUMBITS");
	  break;
  case DEC_ERROR_BITSKIP:
	  printf("DEC_ERROR_BITSKIP");
	  break;
  case DEC_ERROR_BUFFER_OVF:
	  printf("DEC_ERROR_BUFFER_OVF");
	  break;
  }
}

void decoder_print_buffer(decoder_t *dec){
	int idx = (dec->idx+_decoder_buffer_size-1)%_decoder_buffer_size;
	for(int i=0; i<_decoder_buffer_size; i++){
		printf("%d\t%lu\r\n", i>idx?(-_decoder_buffer_size+(i-idx)):(i-idx), dec->time[i]);
	}
	printf("value: %lu\r\n", dec->val);
	printf("---\r\n");
}

void decoder_update(decoder_t *dec, uint32_t value){
	if(dec->val==value) return; // don't save the same value
	if(!dec->enabled) return;
	if(dec->error!=DEC_ERROR_NONE) return; // wait for main to clear error before continuing
	dec->val = value;

	// TIM7 is 16bit timer, take care of overflows
	//const uint16_t tm = htim7.Instance->CNT;
	//const uint16_t tm_last = (uint16_t)dec->last_update;
	const uint16_t tm = HAL_GetTick();
	const uint16_t tm_last = dec->last_update;

	dec->time[dec->idx] = (uint16_t)(tm-tm_last);
	dec->last_update = tm;

	dec->last_update_ms = HAL_GetTick(); // used to reset the decoder

	if(dec->phase==DEC_PH_INIT){
		// check if full header (preamble + pause + data start) found
		//
		// header:
		//  0 0 0 0 0 0 1 0 1 0 1 0 1 0 1 0 1 1 1 1 1 1 0 ...
		//  _ _ _ _ _ _|‾|_|‾|_|‾|_|‾|_|‾|_|‾ ‾ ‾ ‾ ‾|_ (data...)
		//             ^                   ^         ^
		// >10 periods  preamble start      wait      switch to data rx
		//
		// check last 11 entries (each entry is time difference from previous event):
		//   - en(-1) == 5*period
		//   - en(-2)==en(-3)==...==en(-11)==period

		// first, calculate the period from the delay
		const int32_t period_calc = _decoder_entry(dec, 0)/5;
		const int32_t period_tolerance = period_calc/5; // 20% tolerance

		// now, check if last few pulses before the delay match the period
		for(int i=0; i<8; i++){
			const int32_t p = _decoder_entry(dec, -1-i);
			if(p<period_calc-period_tolerance || p>period_calc+period_tolerance || p==0){
				// not good, increase index (continue scanning input)
				dec->idx = (dec->idx+1)%_decoder_buffer_size;
				return;
			}
		}

		/*
		// header seems to be ok. calculate the period again with the whole header to slightly
		// improve precision in case there was some jitter
		uint32_t period = 0;
		for(int i=0; i<8; i++){
			period += _decoder_entry(dec, -1-i);
		}
		period = (period_calc + (period/10))/2;
		dec->period = period;
		*/

		dec->period = period_calc;
		dec->phase = DEC_PH_DATA;

		// initialize data collection
		dec->skip_bits = 1; // skip first bit (part of the header)
		dec->data_idx = 0;
		dec->decode_tmp = 0;
		dec->bit_counter = 0;
		dec->manch_buff = 0xff; // 0xff when empty, last bit value if in the middle of manchester bit
		dec->preamble_found = true; // signal
		dec->error = DEC_ERROR_NONE;
	}

	else if(dec->phase==DEC_PH_DATA){
		// data is manchester encoded -> 01==1, 10==0

		// in valid data stream there could only be one or two same-valued bits between two edge events
		// try to detect those with some tolerance
		int num_bits = 1;
		if(dec->time[dec->idx] > dec->period*3/2) num_bits = 2;
		/*
		if(dec->time[dec->idx] > dec->period*4){
			// probably invalid, reset state machine
			printf("err: %lu\r\n", dec->time[dec->idx]);
			dec->idx = (dec->idx+1)%_decoder_buffer_size;  // update index for easier debugging
			dec->error = DEC_ERROR_NUMBITS;
			// main loop will reinitialize the decoder
			return;
		}
		*/

		for(int i=0; i<num_bits; i++){
			// add bits to temp var
			if(dec->skip_bits>0){
				dec->skip_bits--;
				continue;
			}
			if(dec->data_idx>99){
				// buffer full -> error
				dec->error = DEC_ERROR_BUFFER_OVF;
				// mail loop wil reinitialize the decoder
				return;
			}
			if(dec->manch_buff==0xff){
				// got first half of manchester bit
				dec->manch_buff = dec->val;
				continue;
			}
			if(dec->manch_buff==dec->val){
				// this shouldn't happen, probably a bit skip
				// reset the decoder
				printf("err b: %d\r\n", dec->manch_buff);
				dec->idx = (dec->idx+1)%_decoder_buffer_size;  // update index for easier debugging
				dec->error = DEC_ERROR_BITSKIP;
				// main loop will reinitialize the decoder
				return;
			}
			dec->manch_buff = 0xff;
			dec->decode_tmp |= (dec->val)<<(dec->bit_counter);
			dec->bit_counter++;
			if(dec->bit_counter==8){
				// add to buffer
				dec->data[dec->data_idx++] = dec->decode_tmp;
				dec->bit_counter = 0;
				dec->decode_tmp = 0;
				dec->data_found = true;

				const uint32_t id=dec->time[dec->idx];
				for(int k=0; k<_decoder_buffer_size; k++){
					dec->time[k]=0;
				}
				dec->time[dec->idx]=id;
			}
		}
	}
	dec->idx = (dec->idx+1)%_decoder_buffer_size;
}



void anim_none(bool reset){
	(void) reset;
	return;
}

void anim_circle(bool reset){
	static int phase = 0;
	static int idx = 0;
	if(reset){
		phase = 0;
		idx = 0;
		return;
	}

	switch(phase){
	case 0:
		// light them one by one
		led_status[idx] = 1;
		break;
	case 1:
		led_status[idx] = 0;
		break;
	}

	idx++;
	if(idx>=led_num){
		idx = 0;
		phase ^= 0x01;
	}
}

void anim_blink(bool reset){
	static int phase = 1;
	static int delay = 1;
	if(reset){
		phase = 1;
		delay = 1;
		return;
	}
	delay--;
	if(delay==0){
		if(phase==0){
			set_all_leds(0);
		}
		if(phase==1){
			set_all_leds(1);
		}
		phase ^= 0x01;
		delay = (get_fake_rand()%20)+5;
	}
}

void anim_random(bool reset){
	static int phase = 0;
	static int idx = 0;
	if(reset){
		phase = 0;
		idx = 0;
		return;
	}

	// get the number of empty leds
	idx++;
	if(idx<3) return;
	idx = 0;
	int numblank = 0;
	int val = (phase==0)?0:1;
	for(int i=0; i<led_num; i++){
		if(led_status[i]==val){
			numblank++;
		}
	}
	if(numblank==0){
		// switch phase
		phase ^= 0x01;
		numblank = led_num;
		val ^= 0x01;
	}

	int32_t ridx = get_fake_rand()%numblank;
	for(int i=0; i<led_num; i++){
		if(led_status[i]==val){
			ridx--;
			if(ridx<=0){
				led_status[i] = val^0x01;
				break;
			}
		}
	}
}

void anim_top_to_bot(bool reset){
	static int idx = 0;
	static int phase = 0;
	if(reset){
		idx = 0;
		phase = 0;
		return;
	}
	const uint8_t order[] = {
			14, 15, 13, 16, 12, 11, 17, 10, 18, 9, 19, 20, 8, 21, 7, 22, 6, 23, 5, 24, 25, 4, 3, 2, 1, 0
	};

	led_status[order[idx]] = (phase==0)?1:0;
	idx++;
	if(idx==led_num){
		idx = 0;
		phase ^= 0x01;
	}
}

void anim_bot_to_top(bool reset){
	static int idx = led_num-1;
	static int phase = 0;
	if(reset){
		idx = led_num-1;
		phase = 0;
		return;
	}
	const uint8_t order[] = {
			14, 15, 13, 16, 12, 11, 17, 10, 18, 9, 19, 20, 8, 21, 7, 22, 6, 23, 5, 24, 25, 4, 3, 2, 1, 0
	};

	led_status[order[idx]] = (phase==0)?1:0;
	idx--;
	if(idx==-1){
		idx = led_num-1;
		phase ^= 0x01;
	}
}

typedef void (*anim_func_t)(bool);

const anim_func_t anim_functions[] = {
		anim_random,
		anim_circle,
		anim_blink,
		anim_top_to_bot,
		anim_bot_to_top,
		anim_none,
};

bool __animation_disabled = false;
void animation_update(void){
	static int anim_idx = 0;

	// LEDs used to indicate data rx
	if(light_decoder.phase==DEC_PH_DATA) return;
	if(__animation_disabled) return;

	// animation change
	if(beak_touched){
		beak_touched = false;
		anim_idx++;
		if(anim_idx==sizeof(anim_functions)/sizeof(anim_func_t)) anim_idx=0;
		anim_functions[anim_idx](true);

		// announce change
		for(int i=0; i<5; i++){
			set_all_leds(i%2);
			HAL_Delay(150);
		}
	}
	anim_functions[anim_idx](false);
}


int _touchkey_value = 0;

bool touchkey_check(void){
	static int counter = 0;

	// adjust based on ADC sampling rate
	const int charge_periods = 5;
	const int read_wait_periods = 2;
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	bool update_counter = true;

	if(counter==0){
	  // charge the capacitor using the internal pull-up
	  GPIO_InitStruct.Pin = TOUCH_KEY_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_PULLUP;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	  HAL_GPIO_Init(TOUCH_KEY_GPIO_Port, &GPIO_InitStruct);
	}
	else if(counter==charge_periods){
	  // switch to input mode
	  GPIO_InitStruct.Pin = TOUCH_KEY_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(TOUCH_KEY_GPIO_Port, &GPIO_InitStruct);
	}
	else if(counter==charge_periods+read_wait_periods){
		// read the value
		_touchkey_value = adc_data[1];
		counter = 0;
		update_counter = false;
	}

	if(update_counter) counter++;
	return counter>0;
}

typedef struct{
	uint32_t period;
	uint32_t last;
	uint32_t random;
} refresher_t;

void refresher_init(refresher_t *r, uint32_t period){
	r->period = period;
	r->random = 0;
	r->last = 0;
}

void refresher_init_random(refresher_t *r, uint32_t period, uint32_t randomness){
	r->period = period;
	r->random = randomness;
	r->last = 0;
}

bool refresher_check(refresher_t *r){
	const uint32_t tm = HAL_GetTick();
	if(tm-(*r).last > (*r).period){
		r->last = tm;
		return true;
	}
	return false;
}


void HAL_GPIO_EXTI_Callback(uint16_t pin){
	if(pin==2){
		decoder_update(&ir_decoder, HAL_GPIO_ReadPin(IR_RECV_GPIO_Port, IR_RECV_Pin));
	}
}

void ir_rx_enable(bool en){
	HAL_GPIO_WritePin(IR_RX_EN_GPIO_Port, IR_RX_EN_Pin, en?1:0);
}


void check_ir_rx(void){

}


void ir_send_blocking(const uint8_t *code, int len){
	const uint32_t p = htim1.Init.Period/2;
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, 0);
	HAL_TIM_PWM_Start(&htim1,  TIM_CHANNEL_1);

	// send header
    const uint8_t header[] = {1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,0};
    for(int i=0; i<sizeof(header); i++){
		// send bits one by one
		if(header[i]){
			// high
			__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, p);
		}
		else{
			// low
			__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, 0);
		}
		HAL_Delay(1);
    }

    // send payload
	for(int i=0; i<len; i++){
		for(int b=8; b>0; b--){
			// send one bit (manchester encoded)
			if(code[i] & (1<<(8-b))){
				// high
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, 0);
				HAL_Delay(1);
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, p);
			}
			else{
				// low
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, p);
				HAL_Delay(1);
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, 0);
			}
			HAL_Delay(1);
		}
	}

	// send footer
    for(int i=0; i<16; i++){
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, p);
		HAL_Delay(1);
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, 0);
		HAL_Delay(1);
    }
	HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim){
	if(htim==&htim2){
		led_update_isr();
	}
	else if(htim==&htim7){
		// used for data detection timing
	}
}

int edge = -1;

#define ADC_LS_DATA_DUMP 0
#if ADC_LS_DATA_DUMP
uint8_t adc_data_buffer[300] = {0};
int adc_data_buffer_idx = 0;
bool sampling = true;
#endif

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc){

	// Photo sensor edge detection
	// Due to the possible differences in ambient lighting conditions
	// we can't use fixed threshold.
	// But, according to experiments, high levels are consistent.
	// Rising edge detection first detects when the signal crossed the
	// high level threshold and then follows the signal back to the local
	// minimum. Falling edge detection works in a similar way - it detects
	// the crossing of the lower level threshold and then follows the signal
	// back to the local maximum. Due to this the whole signal is delayed
	// for $buffersize samples

	// buffer for last 20 samples
	const int16_t histsize = 20;
	static int16_t hist[20] = {0}; // histsize
	static int histidx = 0;

	static int current = 0;

	// delays
	static int rising_after = histsize;
	static int falling_after = histsize;

	const uint16_t thr_low = 180;
	const uint16_t thr_high = 200;

	histidx = (histidx+1)%histsize;
	hist[histidx] = adc_data[0];

	if(current==0){
		// look for rising edge
		if(hist[histidx] > thr_high){
			int offset = histsize-1;
			for(int i=1; i<histsize; i++){
				if(hist[(histidx+histsize-i)%histsize]-hist[(histidx+histsize-i-1)%histsize] <= 0){
					if(i<histsize-1 && hist[(histidx+histsize-i)%histsize]-hist[(histidx+histsize-i-2)%histsize] > 0){
						// on the way back there can be samples higher than the current one (noise) and they last
						// just one sample so we also check the sample before to see if we're actually at the minimum
						// or is it just noise. On a noisy signal this can slightly negatively affect the edge measurement.
						continue;
					}
					offset = i;
					break;
				}
			}
			if(hist[(histidx+histsize-offset)%histsize] < thr_low){
				// rising edge detected
				current = 1;
				// compensate the delay
				rising_after = histsize-offset;
			}
		}
	}
	else{
		// look for falling edge
		if(hist[histidx] < thr_low){
			int offset = histsize;
			for(int i=1; i<histsize-1; i++){
				if(hist[(histidx+histsize-i)%histsize]-hist[(histidx+histsize-i-1)%histsize] >= 0){
					offset = i;
					break;
				}
			}
			if(hist[(histidx+histsize-offset)%histsize] > thr_high){
				current = 0;
				// compensate the delay
				falling_after = histsize-offset;
			}
		}
	}

	if(rising_after>0){
		rising_after--;
		if(rising_after==0){
			// trigger rising edge
			edge = 1;
			decoder_update(&light_decoder, 1);
			if(light_decoder.phase==DEC_PH_DATA){
				// make sure LEDs in low part are off. Upper LEDs are for data indication
				set_all_leds(0);
			}
		}
	}

	if(falling_after>0){
		falling_after--;
		if(falling_after==0){
			// trigger falling edge
			edge = 0;
			decoder_update(&light_decoder, 0);
			if(light_decoder.phase==DEC_PH_DATA){
				// make sure LEDs in low part are off. Upper LEDs are for data indication
				set_all_leds(0);
				for(int i=8; i<=16; i++){
					led_status[i] = 1;
				}
			}
		}
	}

#if ADC_LS_DATA_DUMP
	// record the light sensor data
	if(sampling){
		adc_data_buffer[adc_data_buffer_idx++] = adc_data[0];
		if(adc_data_buffer_idx==sizeof(adc_data_buffer)){
			adc_data_buffer_idx = 0;
			sampling = false;
		}
	}
#endif

	// run the touchkey_check every 300 periods
	static uint32_t touchkey_refresh = 0;
	if(touchkey_refresh>300){
		if(!touchkey_check()){
			touchkey_refresh = 0;
		}
	}
	touchkey_refresh++;
}



bool validate_data_frame(decoder_t *dec){
	// data frame format:
    // <frame_start_mark+flags><data_length><data><checksum>
	if(dec->data_idx<4 || dec->data[1]>95) return false;
	if((dec->data[0]&0xf)!=0xa) return false; // check frame start mark
	if(dec->data[1]+3>dec->data_idx) return false; // check length (prevent leet haxors from overflowing the buffer)

	uint8_t checksum = 0;
	for(int i=0; i<dec->data[1]+2; i++){
		checksum += dec->data[i];
	}
	return checksum==dec->data[dec->data[1]+2];
}


void pack_data_frame(dev_flags_t flags, uint8_t *data, size_t data_size, uint8_t *frame, size_t *frame_size){
	*frame_size = 0;
	frame[(*frame_size)++] = 0xa | flags;
	frame[(*frame_size)++] = data_size;
	for(int i=0; i<data_size; i++){
		frame[(*frame_size)++] = data[i];
	}
	uint32_t checksum = 0;
	for(int i=0; i<data_size+2; i++){
		checksum += frame[i];
	}
	frame[(*frame_size)++] = checksum&0xff;
}


void disp_turn_on_and_clear(void){
	printf("display turn on\r\n");
	DEV_Module_Init();
	printf("display init and clear...\r\n");
	DisplayInit();
	DisplayClear();
	HAL_Delay(100);
	DisplayClear();
	HAL_Delay(500);

	printf("Paint_NewImage\r\n");
	Paint_NewImage(BlackImage, DISPLAY_WIDTH, DISPLAY_HEIGHT, 270, WHITE);
	Paint_Clear(WHITE);
#ifdef EINK_DISPLAY_2IN66_TWOCOLOR
	Paint_NewImage(RedImage, DISPLAY_WIDTH, DISPLAY_HEIGHT, 270, WHITE);
	Paint_Clear(WHITE);
#endif
}

void disp_turn_off(void){
	// this is done in the display_update
	//printf("Goto Sleep...\r\n");
	//DisplaySleep();
	//DEV_Delay_ms(2000); //important, at least 2s
	printf("close 5V, Module enters 0 power consumption ...\r\n");
	DEV_Module_Exit(); // power off
}

void disp_print_text(const char *txt, int x, int y){
	printf("Drawing text\r\n");

	// check where to wrap
	const int available_x = DISPLAY_HEIGHT-x;
	const int available_y = DISPLAY_WIDTH-y;

	// find first space character before the wrap and break it there
	int last_space = 0;
	int tot_width = 0;
	int start = 0;
	char buffer[80] = "";
	int row = 0;
	const int margin = 3;

	for(int i=0; i<strlen(txt); i++){
		if(txt[i]==' ') last_space = i;

		tot_width += Font24.Width;
		if(tot_width>available_x){
			if(last_space!=0){
				// break on last space character
				int bi = 0;
				for(int c=start; c<last_space; c++){
					buffer[bi++] = txt[c];
				}
				buffer[bi] = '\0';
				start = last_space+1;
				tot_width = 0;
				for(int c=last_space+1; c<i; c++){
					tot_width += Font24.Width;
				}
				last_space = 0;
			}
			else{
				// break here
				int bi = 0;
				for(int c=start; c<i; c++){
					buffer[bi++] = txt[c];
				}
				buffer[bi] = '\0';
				start = i;
				tot_width = 0;
			}
			Paint_DrawString_EN(x, y+row*Font24.Height+row*margin, buffer, &Font24, WHITE, BLACK);
			printf("x: %d, y: %d, '%s'\r\n", x, y+row*Font24.Height+row*margin, buffer);
			row++;
			if(row*Font24.Height+row*margin > available_y){
				return;
			}
		}
	}

	if(start<strlen(txt)-1){
		Paint_DrawString_EN(x, y+row*Font24.Height+row*margin, txt+start, &Font24, WHITE, BLACK);
		printf("x: %d, y: %d, '%s'\r\n", x, y+row*Font24.Height+row*margin, txt+start);
	}
}

void disp_print_small_text(const char *txt, int x, int y){
	printf("Drawing text\r\n");
	Paint_DrawString_EN(x, y, txt, &Font12, WHITE, BLACK); // width 7px
}

void disp_apply(void){
#ifdef EINK_DISPLAY_2IN66_MONOCHROMATIC
	DisplayShow(BlackImage);
#else
	DisplayShow(BlackImage, RedImage);
#endif
}

bool parse_dev_options(decoder_t *dec, device_config_t *config){
	// check the header byte for flags
	bool change = false;
	uint8_t flags = dec->data[0];

	dev_flags_t df[] = {DEVFLAG_HIDE_QUEST, DEVFLAG_IR_SEND_DISABLE, DEVFLAG_IR_RECV_DISABLE};
	for(int i=0; i<3; i++){
		const dev_flags_t f = df[i];

		if((flags&f) != (config->flags&f)){
			change = true;
			if(flags&f){
				// set
				config->flags |= f;
			}
			else{
				// clear
				config->flags &= ~f;
			}
		}
	}

	return change;
}

bool handle_data_frame(decoder_t *dec, device_config_t *config){
	bool change = false;

	if(dec->data[1]==1){
		// if the payload has only one element it's the ID of the sender
		// check if it's one of the "special" IDs (0-12) and update the quest
		// image. Otherwise just update the badge counter
		uint8_t sender = dec->data[2];

		if(sender==0xfe){
			// test signal, just blink the LEDs
			printf("test signal received\r\n");
			if(dec==&ir_decoder){
				// data received through light sensor will already indicate validity
				__animation_disabled = true;
				animation_update();
				set_all_leds(0);
				HAL_Delay(200);
				for(int i=0; i<4; i++){
					set_all_leds(1);
					HAL_Delay(200);
					set_all_leds(0);
					HAL_Delay(200);
				}
				__animation_disabled = false;
			}
			return false;
		}

		// IDs [1, 12] unlock the quest fields (including our own)
		if(sender>=1 && sender<=12){
			// check if already open
			if(!(config->quest_status&(1U<<sender))){
				change = true;
				config->quest_status ^= (1U<<sender);
			}
		}

		if(sender!=__INSTANCE_ID && sender!=0){
			// don't count the device itself or the default device
			// seen devices are stored as a bit array inside config->seen_devices
			const int dev_idx = sender/32;
			const int bit_idx = sender-(sender/32)*32;
			// check if already seen
			const bool seen = config->seen_devices[dev_idx]&(1U<<bit_idx);
			if(!seen){
				change = true;
				config->seen_devices[dev_idx] |= (1U<<bit_idx);
			}
		}
	}
	else if(dec->data[1]==2 && dec->data[2]==0xff && dec->data[3]>=1 && dec->data[3]<=4){
		// if the payload has two elements {0xff, 1-4} then it's one of the special codes
		// unlocking the bonus elements.
		const int bit_idx = dec->data[3]-1+28;
		if(!(config->quest_status&(1U<<bit_idx))){
			change = true;
			config->quest_status |= (1U<<bit_idx);
		}
	}
	else if(dec->data[1]==2 && dec->data[2]==0xde && dec->data[3]==0xad && dec==&light_decoder){
		// reset configuration
		// WARNING: use carefully, only available on light input
		configuration_init(config);
		change = true;
	}
	else{
		printf("invalid data frame: ");
		for(int i=0; i<dec->idx; i++){
			printf("0x%02X ", dec->data[i]);
		}
		printf("\r\n");
		return false;
	}

	return change;
}

uint32_t _display_off_after = 0;
void display_update(device_config_t *config){
	int left = 120;
	// initialize display
	disp_turn_on_and_clear();

	if(!(config->flags&DEVFLAG_HIDE_QUEST)){
		Paint_DrawBitMap_Paste(gImage_image, 1, 0, 114, 152, 1);
		for(int i=0; i<3; i++){
			for(int j=0; j<4; j++){
				const int bidx = i*4+j+1;
				printf("i %d, j %d , bl %d ", i, j, bidx);
				if(!(config->quest_status & (1<<bidx))){
					Paint_DrawBitMap_Paste(gImage_block, i*38+1, j*38, 38, 38, 1);
					printf("block");
				}
					printf("\r\n");
			}
		}

		// add bonus items
		int bonus = 1;
		if(config->quest_status&(1<<31)){
			Paint_DrawBitMap_Paste(gImage_save, DISPLAY_HEIGHT-bonus*20, 0, 16, 16, 1);
			bonus++;
		}
		if(config->quest_status&(1<<30)){
			Paint_DrawBitMap_Paste(gImage_robot, DISPLAY_HEIGHT-bonus*20, 0, 16, 16, 1);
			bonus++;
		}
		if(config->quest_status&(1<<29)){
			Paint_DrawBitMap_Paste(gImage_thumbup, DISPLAY_HEIGHT-bonus*20, 0, 16, 16, 1);
			bonus++;
		}
		if(config->quest_status&(1<<28)){
			Paint_DrawBitMap_Paste(gImage_heart, DISPLAY_HEIGHT-bonus*20, 0, 16, 16, 1);
			bonus++;
		}

		// add badge count
		int tot_dev = 0;
		for(int devidx=0; devidx<8; devidx++){
			for(int bitidx=0; bitidx<32; bitidx++){
				if(config->seen_devices[devidx]&(1U<<bitidx)){
					tot_dev++;
				}
			}
		}
		char tot_txt[25] = "";
		sprintf(tot_txt, "total badge count: %d", tot_dev);
		disp_print_small_text(tot_txt, DISPLAY_WIDTH-(strlen(tot_txt)/7)-1, 140);
	}
	else{
		left = 10;
	}

	disp_print_text((char*)config->text, left, 40);

	disp_apply();
	printf("Goto Sleep...\r\n");
	DisplaySleep();
	_display_off_after = HAL_GetTick()+2500; // important, at least 2s before powering off
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI1_Init();
  MX_TIM1_Init();
  MX_USART2_UART_Init();
  MX_TIM2_Init();
  MX_ADC1_Init();
  MX_TIM7_Init();
  /* USER CODE BEGIN 2 */

  // start TIM7 (for time measurement)
  // with CPU freq of 32MHz and TIM7 prescaler of 63
  // each tick is equal to 2us with the overflow happening
  // every 131ms
  HAL_TIM_Base_Start_IT(&htim7);

  HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
  HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc_data, 2);

  // start LED matrix disabled
  for(int i=0; i<led_cols_num; i++){
	  HAL_GPIO_WritePin(led_cols[i].port, led_cols[i].pin, 1);
  }
  for(int i=0; i<led_rows_num; i++){
	  HAL_GPIO_WritePin(led_rows[i].port, led_rows[i].pin, 0);
  }

  // the pre-build script update_instance_id.py creates an instance_id.h file
  // instance_id.txt file can be used to set __INSTANCE_ID. Adding " inc" after the
  // id will cause it to increase after every build.
  // if there's no instance_id.txt file the __INSTANCE_ID will be 0

  // reset leds
  printf("boot\r\n");
  printf("firmware version: %s\r\n", __GIT_VERSION);
  printf("build date: %s\r\n", __BUILD_DATE);
  printf("instance ID: 0x%X\r\n", __INSTANCE_ID);
  set_all_leds(0);

  // initialize data decoders
  decoder_init(&light_decoder);
  decoder_init(&ir_decoder);

  // start timer interrupt
  HAL_TIM_Base_Start_IT(&htim2);

  // load configuration
  device_config_t configuration;
  if(!configuration_load(&configuration)){
	  configuration_init(&configuration);
  }
  configuration_print(&configuration);

  // load device UID
  const uint32_t uid[3] = {*(__IO uint32_t *)(UID_BASE), *(__IO uint32_t *)(UID_BASE+2), *(__IO uint32_t *)(UID_BASE+4)};
  printf("uid: %lX-%lX-%lX\r\n", uid[0], uid[1], uid[2]);

  // boot animation
  for(int idx = 0; idx<led_num; idx++){
	  led_status[idx] = 1;
	  HAL_Delay(50);
  }

  display_update(&configuration);

  // enable the IR receiver
  if(!(configuration.flags&DEVFLAG_IR_RECV_DISABLE)){
	  ir_rx_enable(true);
  }

  // initialize refreshers (poor man's scheduler)
  refresher_t animation_refresh;
  refresher_init(&animation_refresh, 50);

  refresher_t ir_read_refresh;
  refresher_init(&ir_read_refresh, 100);

  refresher_t ir_tx_refresh;
  refresher_init(&ir_tx_refresh, 80000);

  uint32_t beak_touched_last = 0;
  const uint32_t beak_touched_min_period = 3000;

  // helper variables to easily iterate decoders
  decoder_t *decoders[2] = {&light_decoder, &ir_decoder};
  const char *dec_names[10] = {"light", "IR"};

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */


	  if(refresher_check(&animation_refresh)){
		  animation_update();
	  }

	  if(_display_off_after!=0 && HAL_GetTick()>_display_off_after){
		  _display_off_after = 0;
		  disp_turn_off();
	  }

	  if(refresher_check(&ir_tx_refresh)){
		  printf("IR tx\r\n");
		  uint8_t data[] = {__INSTANCE_ID,};
		  uint8_t frame[16];
		  size_t frame_size=0;
		  pack_data_frame(DEVFLAG_IS_CONFIG, data, sizeof(data), frame, &frame_size);
		  printf("IR frame to send: ");
		  for(int i=0; i<frame_size; i++){
			  printf("%02x ", frame[i]);
		  }
		  printf("\r\n");
		  ir_send_blocking(frame, frame_size);
	  }

	  if(refresher_check(&ir_read_refresh)){
		  //check_ir_rx();

		  //printf("touch key: %d\r\n", _touchkey_value);

		  if(_touchkey_value>=2 && HAL_GetTick()-beak_touched_last > beak_touched_min_period){
			  beak_touched = true;
		  }

		  // reset decoders when idle for more than 200ms while in RX mode
		  for(int didx=0; didx<2; didx++){
			  if(HAL_GetTick() - decoders[didx]->last_update_ms > 200 && decoders[didx]->phase==DEC_PH_DATA){
				  printf("reset dec %s\r\n", dec_names[didx]);
				  decoder_init(decoders[didx]);
			  }
		  }

#if ADC_LS_DATA_DUMP
		  // dump light sensor data to serial
		  if(!sampling){
			  for(int i=0; i<sizeof(adc_data_buffer); i++){
				  printf("%3d, ", adc_data_buffer[i]);
			  }
			  printf("\n");
			  sampling=true;
		  }
#endif // ADC_LS_DATA_DUMP

	  }

	  for(int didx=0; didx<2; didx++){
		  decoder_t *dec = decoders[didx];
		  const char *dec_name = dec_names[didx];

		  if(dec->preamble_found){
			  printf("PREAMBLE found on %s input, period: %lu\r\n", dec_name, dec->period);
			  dec->preamble_found = false;
		  }
		  if(dec->data_found){
			  printf("got data from %s sensor\r\n", dec_name);
			  dec->data_found = false;
			  for(int i=0; i<dec->data_idx; i++){
				  printf("%02x ", dec->data[i]);
			  }
			  printf("\r\n");

			  if(validate_data_frame(dec)){
				  dec->enabled = false; // prevent updating

				  // confirmation blink only for valid light frame
				  if(dec==&light_decoder){
					  animation_update();
					  set_all_leds(0);
					  HAL_Delay(200);
					  for(int i=0; i<4; i++){
						  set_all_leds(1);
						  HAL_Delay(200);
						  set_all_leds(0);
						  HAL_Delay(200);
					  }
				  }

				  printf("valid %s frame received on %s sensor!\r\n", dec->data[0]&DEVFLAG_IS_CONFIG?"config":"data", dec_name);
				  if(!(dec->data[0]&DEVFLAG_IS_CONFIG)){
					  // text frame
					  char t[80] = {0};
					  for(int i=0; i<dec->data[1]; i++){
						  t[i] = dec->data[i+2];
					  }
					  t[dec->data[1]] = 0;
					  printf("got text: %s\r\n", t);

					  bool change = parse_dev_options(dec, &configuration);
					  if(strcmp(t, (char*)configuration.text)!=0){
						  change = true;
						  strncpy((char*)configuration.text, t, 100);
					  }

					  if(change){
						  configuration_save(configuration);
						  display_update(&configuration);
					  }
				  }
				  else{
					  printf("got data: ");
					  for(int i=0; i<dec->data[1]; i++){
						  printf("0x%02X ", dec->data[i+2]);
					  }
					  printf("\r\n");
					  bool change = handle_data_frame(dec, &configuration);
					  change |= parse_dev_options(dec, &configuration);
					  if(change){
						  configuration_save(configuration);
						  display_update(&configuration);
					  }
				  }
				  decoder_init(dec); 	// get data and reset
			  }
		  }
		  if(dec->error!=DEC_ERROR_NONE){
			  printf("%s decoder error: ", dec_name);
			  print_decoder_error(dec->error);
			  printf("\r\n");
			  decoder_print_buffer(dec);
			  decoder_init(dec);
			  if(dec==&light_decoder){
				  set_all_leds(0);
			  }
		  }
	  }

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 2;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV32;
  hadc1.Init.Resolution = ADC_RESOLUTION_8B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.NbrOfConversion = 2;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_11;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_92CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_9;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  sConfig.Offset = 1;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 860;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 7199;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 64;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 65535;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LED_ROW_1_Pin|EPD_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, IR_RX_EN_Pin|LED_ROW_3_Pin|LED_ROW_4_Pin|LED_COL_2_Pin
                          |LED_COL_3_Pin|LED_COL_4_Pin|LED_COL_1_Pin|LED_ROW_2_Pin
                          |EPD_RST_Pin|EPD_DC_Pin|LED_COL_7_Pin|LED_COL_6_Pin
                          |LED_COL_5_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : LED_ROW_1_Pin SPI1_CS_Pin EPD_EN_Pin */
  GPIO_InitStruct.Pin = LED_ROW_1_Pin|SPI1_CS_Pin|EPD_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : IR_RECV_Pin */
  GPIO_InitStruct.Pin = IR_RECV_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(IR_RECV_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : IR_RX_EN_Pin LED_ROW_3_Pin LED_ROW_4_Pin LED_COL_2_Pin
                           LED_COL_3_Pin LED_COL_4_Pin LED_COL_1_Pin LED_ROW_2_Pin
                           EPD_RST_Pin EPD_DC_Pin LED_COL_7_Pin LED_COL_6_Pin
                           LED_COL_5_Pin */
  GPIO_InitStruct.Pin = IR_RX_EN_Pin|LED_ROW_3_Pin|LED_ROW_4_Pin|LED_COL_2_Pin
                          |LED_COL_3_Pin|LED_COL_4_Pin|LED_COL_1_Pin|LED_ROW_2_Pin
                          |EPD_RST_Pin|EPD_DC_Pin|LED_COL_7_Pin|LED_COL_6_Pin
                          |LED_COL_5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : EPD_BUSY_Pin */
  GPIO_InitStruct.Pin = EPD_BUSY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(EPD_BUSY_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : BOOT0_Pin */
  GPIO_InitStruct.Pin = BOOT0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BOOT0_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
