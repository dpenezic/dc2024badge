/*
 * DEV_Config.c
 *
 *  Created on: Mar 6, 2024
 *      Author: i
 */


// mostly copied from the Waveshare's DEV_Config.c file in order to adjust
// properties without having to change other Waveshare files

#include "DEV_Config.h"

extern SPI_HandleTypeDef hspi1;
void DEV_SPI_WriteByte(UBYTE value){
    HAL_SPI_Transmit(&hspi1, &value, 1, 1000);
}

void DEV_SPI_Write_nByte(UBYTE *value, UDOUBLE len){
    HAL_SPI_Transmit(&hspi1, value, len, 1000);
}

int DEV_Module_Init(void){
    DEV_Digital_Write(EPD_DC_PIN, 0);
    DEV_Digital_Write(EPD_CS_PIN, 0);
	DEV_Digital_Write(EPD_PWR_PIN, 0); // pull low to enable EPD power
    DEV_Digital_Write(EPD_RST_PIN, 1);
	return 0;
}

void DEV_Module_Exit(void){
    DEV_Digital_Write(EPD_DC_PIN, 0);
    DEV_Digital_Write(EPD_CS_PIN, 0);

	DEV_Digital_Write(EPD_PWR_PIN, 1); // pull high to disable EPD power
    DEV_Digital_Write(EPD_RST_PIN, 0);
}

