# [DORS/CLUC 2024](https://www.dorscluc.org/) electronic badge

This repository contains the source code for the DORS/CLUC 2024 electronic badge.

![Badge](badge.jpg)

Repository structure:
- `hardware/`: Hardware design files for the badge
- `firmware/`: Firmware for the badge
- `web/`: Web page for badge configuration
- `badge_stand/`: FreeCAD model and STL file for badge stand
- `programmer/`: FreeCAD model and STL files for quick and dirty batch programming jig

Check each directory for licensing information.


### Usage

Usage instructions and more info is available here: [https://www.dorscluc.org/badge/](https://www.dorscluc.org/badge/)


### Open source hardware

THe device is certified as open source hardware under the [OSHWA certification program](https://certification.oshwa.org/hr000116.html) with a mark HR000116.


